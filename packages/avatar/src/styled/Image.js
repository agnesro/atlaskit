import styled from 'styled-components';

export default styled.img`
  display: inline-block;
  height: 100%;
  width: 100%;
`;
