import {
  akColorN900,
  akColorN20,
  akColorN40,
  akColorN90,
  akColorN400,
  akColorG400,
  akColorB500,
  akColorY500,
  akColorR500,
  akColorT300,
  akColorT500,
  akColorP500,
} from '@atlaskit/util-shared-styles';

import { Theme } from './themeBuilder';

export const defaultColors: Theme = {
  lineNumberColor: akColorN90,
  lineNumberBgColor: akColorN40,
  backgroundColor: akColorN20,
  textColor: akColorN900,
  substringColor: akColorN400,
  keywordColor: akColorP500,
  attributeColor: akColorT500,
  selectorTagColor: akColorP500,
  docTagColor: akColorY500,
  nameColor: akColorP500,
  builtInColor: akColorP500,
  literalColor: akColorP500,
  bulletColor: akColorP500,
  codeColor: akColorP500,
  additionColor: akColorP500,
  regexpColor: akColorT300,
  symbolColor: akColorT300,
  variableColor: akColorT300,
  templateVariableColor: akColorT300,
  linkColor: akColorB500,
  selectorAttributeColor: akColorT300,
  selectorPseudoColor: akColorT300,
  typeColor: akColorT500,
  stringColor: akColorG400,
  selectorIdColor: akColorT500,
  selectorClassColor: akColorT500,
  quoteColor: akColorT500,
  templateTagColor: akColorT500,
  deletionColor: akColorT500,
  titleColor: akColorR500,
  sectionColor: akColorR500,
  commentColor: akColorN400,
  metaKeywordColor: akColorG400,
  metaColor: akColorN400,
  functionColor: akColorG400,
  numberColor: akColorB500
};
