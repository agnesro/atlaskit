# Emoji

Use emoji....

## Try it out

Interact with a [live demo of the @NAME@ component](https://aui-cdn.atlassian.com/atlaskit/stories/@NAME@/@VERSION@/).

## Installation

```sh
npm install @NAME@
```

## Using the component

Import the component in your React app as follows:

```js
import EmojiPicker from '@NAME@';
ReactDOM.render(<EmojiPicker />, container);
```

## Storybook

The storybook includes a set of stories for running against a live server. See ```ak-emoji/external-emoji```.

You can specify the URL's manually in the textarea on the story (as json configuration suitable for EmojiResource),
or specify it when running story in the local-config.json in the root of this component.

There is an example file local-config-example.json that can be copied.
