/* tslint:disable:variable-name */
import styled from 'styled-components';

export const DropdownWrapper = styled.div`
  background: white;
  border-radius: 3px;
  overflow: hidden;
  display: block;
`;

export default DropdownWrapper;
