import styled from 'styled-components';

const GlobalNavigationOuter = styled.div`
  position: relative;
`;

GlobalNavigationOuter.displayName = 'GlobalNavigationOuter';
export default GlobalNavigationOuter;
