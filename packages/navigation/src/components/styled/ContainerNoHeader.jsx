import styled from 'styled-components';
import { container } from '../../shared-variables';

const ContainerNoHeader = styled.div`
  height: ${container.padding.top}px;
`;

ContainerNoHeader.displayName = 'ContainerNoHeader';

export default ContainerNoHeader;
