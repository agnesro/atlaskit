import styled from 'styled-components';

const GlobalNavigationPrimaryContainer = styled.div`
  flex: 1 1 0;
`;

GlobalNavigationPrimaryContainer.displayName = 'GlobalNavigationPrimaryContainer';
export default GlobalNavigationPrimaryContainer;
