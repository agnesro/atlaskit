export const URL_REGEX = /\b(((https?|ftps?):\/\/|(www\.))[a-zA-Z\u00a1-\uffff0-9\.\$\-_\+!\*',\/\?:@=&%#~;]+)/;
export const EMAIL_REGEX = /\b([a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+\.([a-zA-Z0-9-]+))/;
