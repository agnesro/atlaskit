import { style } from 'typestyle';

export const triggerWrapper = style({
  display: 'flex',
});

export const expandIcon = style({
  marginLeft: -8,
});
